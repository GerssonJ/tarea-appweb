create database RentaCarro

create table Maestro(
  id_maestro int IDENTITY(1,1) PRIMARY KEY,
  categoria varchar(30) not null
 );

 insert into Maestro(categoria)values('Camioneta');

 select * from detalle_Vehiculo;
 create table detalle_Vehiculo(
   id_detalle int IDENTITY(1,1) PRIMARY KEY,
   marca varchar(50) not null,
   modelo int not null,
   color varchar(50)not null,
   Tipo_Transmicion varchar(50)not null,
   Tipo_Combustible varchar(50)not null,
   id_maestro int not null
 );

 drop table detalle_Vehiculo;
 go
  
  alter table detalle_Vehiculo add constraint FK_Union foreign key (id_maestro)references Maestro( id_maestro) on delete cascade on update cascade;
  go
  
 insert into detalle_Vehiculo(marca,modelo,color,Tipo_Transmicion,Tipo_Combustible,id_maestro)values('Toyota-Yaris',2014,'Negro','Manual','Gasolina',1);
 

 ALTER TABLE detalle_Vehiculo  
DROP CONSTRAINT FK_Union; 


  select * from detalle_Vehiculo join Maestro on detalle_Vehiculo.id_maestro=Maestro.id_maestro;

  go
 
  create procedure Automivil
  as
  select * from detalle_Vehiculo where id_maestro=1;
  
  go
  create procedure Moto
  as
 select * from detalle_Vehiculo where id_maestro=2;

  go
  create procedure Camioneta
  as
  select * from detalle_Vehiculo where id_maestro=3;